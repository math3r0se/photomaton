import os
import cv2
import time
import random
from PIL import Image

def watermark_photo(input_image, output_image, watermark_image, position):
    base_image = Image.open(input_image)
    watermark = Image.open(watermark_image)

    base_image.paste(watermark, position, mask=watermark)
    base_image.save(output_image)

cam = cv2.VideoCapture(0)
cv2.namedWindow("Photomaton")

img_counter = 0

import random

s = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
passlen = 25

while True:
    ret, frame = cam.read()
    cv2.imshow("Capture", frame)
    if not ret:
        break
    k = cv2.waitKey(1)

    if k%256 == 27:     #Si Echap est appuye
        print("Fermeture...")
        break
    elif k%256 == 32:   #Si Espace est appuye
        print("3..")
        time.sleep(1)
        print("2..")
        time.sleep(1)
        print("1..")
        time.sleep(1)
        img_name = "Photo_n_{}.png".format(img_counter)
        cv2.imwrite(img_name, frame)
        print("Photo prise !".format(img_name))
        p = "".join(random.sample(s, passlen))
        watermark_photo(img_name, '{}.png'.format(p), 'SDV.png', position=(15,15))
        print ("Votre identifiant photo est le " + p)
        os.remove(img_name)
        img_counter+=1

cam.release()
cv2.destroyAllWindows()
